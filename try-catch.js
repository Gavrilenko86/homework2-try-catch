const rootDiv = document.createElement("div");
rootDiv.id = "root";
document.body.appendChild(rootDiv);

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70,
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70,
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70,
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40,
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    },
];

const ul = document.createElement("ul");

books.forEach((book) => {
    const { author, name, price } = book;
    try {
        if (author && name && price) {
            const li = document.createElement("li");
            li.textContent = `${author}: ${name} - ${price} грн`;
            ul.appendChild(li);
        } else {
            if (!author) {
                throw new Error("No author");
            } else if (!name) {
                throw new Error("No name");
            } else if (!price) {
                throw new Error("No price");
            }
        }
    } catch (e) {
        console.error(e);
    }
});

rootDiv.appendChild(ul);
